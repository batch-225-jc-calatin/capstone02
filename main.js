const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv").config();


// Connect Routes

const userRoutes = require("./routes/userRoutes");
const productRoutes = require ("./routes/productRoutes");
const orderRoutes = require ("./routes/orderRoutes");


// Connect Express

const app = express();
const port = 3010;
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Connect Mongoose

mongoose.connect(`mongodb+srv://jc0713:${process.env.PASSWORD}@cluster0.lkllag4.mongodb.net/capstone?retryWrites=true&w=majority`, 
	{

	useNewUrlParser: true,
	useUnifiedTopology: true
	
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.on('open', () => console.log('Connected to MongoDB'));


app.use("/users", userRoutes);
app.use("/product", productRoutes);
app.use("/order", orderRoutes);


// app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`API is now online on port ${port}`));
