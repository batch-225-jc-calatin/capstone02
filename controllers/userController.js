const User = require("../models/user");

const bcrypt = require("bcrypt");

const auth = require("../auth");

const Product = require("../models/product");



module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true
		}
		return false
	})
}

module.exports.getAllActive = () => {
	 return User.find({isAdmin : true}).then(result => {
	 	return result;
	 }) ;
}


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10) 
	})


return newUser.save().then((user, error) => {
	if (error) {
		return false
	} else {
		return user		
	};
});

};


module.exports.loginUser = (data) => {
	return User.findOne({email : data.email}).then(result => {
		if(result == null ){
			return {
				message: "Not found in our database"
			}
		} else {
			const isPasswordCorrect = bcrypt.compareSync(	data.password, result.password);
			if (isPasswordCorrect) {
				return {access : auth.createAccessToken(result)}

			} else {
				return {
						message: "password was incorrect"
				}
			};

		};

	});
};

module.exports.getAllAdmin = () => {
	return User.find({isAdmin : true}).then(result => {
		return result
	});
};

module.exports.getAdmin = () => {
	return User.findOne({}).then(result => {
		if(result = true){
			return  result
		} else {
			message : " Hi User"
		}
		
	});
};


module.exports.getSingleUser = (data) => {

	return User.findById(data.userId).then(result => {
result.password = "";

		
		return result;

	});

};


module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({productId : data.productId});
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true
			};
		});
	}); 
	
	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.enrollees.push({userId : data.productId});

		return product.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

	if(isUserUpdated && isProductUpdated){
		return {
			message: "Complete Purchased"
		}
	} else {
		return false;
	};


}

module.exports.getProfile = (userData) => {
		return User.findById({id : userData.id}).then(result => {
			if (result == null){
				return false
			} else {
				result.password = ""
				return result
			}
		});
	};

module.exports.getDetails= (data) => {

	 return User.findById(data.userId).then(result => {
	 	return result;
	 }) ;
};