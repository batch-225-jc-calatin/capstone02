const Product = require("../models/product");


module.exports.addProduct = (data) => {
	if(data.isAdmin){
		let new_product = new Product ({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

		return new_product.save().then((new_product, error) => {
			if(error){
				return false
			}

			return {
				message: 'New Product successfully created!'
			}
		})
	} 

	let message = Promise.resolve('User must me Admin to Access this.')

	return message.then((value) => {
		return value

	// you can use this to simplify code
	// Promise.resolve('User must me Admin to Access this.')
	});
		
};

module.exports.checkProductExists = (reqBody) => {

	return Product.find({name : reqBody.name}).then(result => {

		// the find method returns a record if a match is found

		if (result.length > 0) {

			return true
		}

		return false
	})
}

// Retrieve All Product

module.exports.getAllProduct = (data) => {
	return Product.find({}).then(result => {
		return result;
	});
}

module.exports.productActive = (data) => {

	 return Product.find({isActive : true}).then(result => {
	 	return result
	 }) ;
}

module.exports.getProduct= (data) => {

	 return Product.findById(data.productId).then(result => {
	 	return result;
	 }) ;
}

// Retrive One Product

module.exports.getSingle = (data) => {

	return Product.findById(data.userId).then(result => {
result.password = "";

		
		return result;

	});

};




// Product Update

module.exports.updateProduct = (reqParams, reqBody) => {

	

	let updateProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	


	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product,error) => {

		if (error) {

			return false;

		} else {

			return {
				message: "Product updated Succesfully"
			}
		}
	})
};




// Archieve Product

module.exports.archieveProduct = (reqParams) => {
	let updateActiveField = {

		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {

			return false;
		} else {

			return {
				message :"archieving Product Successfully"
			}
		};
	});
};



module.exports.unarchieveProduct = (reqParams) => {
	let updateActiveField = {

		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {

			return true;
		} else {

			return {
				message :"archieving Product Successfully"
			}
		};
	});
};




// In Archieve Product






