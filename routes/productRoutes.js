const express = require("express");
const router = express.Router();
const auth = require("../auth");

const productController = require("../controllers/productController");

// Add Product 

router.post("/addProduct", auth.verify,  (req, res) => {

	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(result => res.send(result));
});



router.get("/", (req, res) => { 

	productController.getAllProduct().then(result => res.send(result));

});


router.get("/getActive", (req, res) => {

	productController.productActive().then(result => res.send(result));
});


router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(result => res.send(result));
});

router.post("/checkProduct", (req, res) => {
	productController.checkProductExists(req.body).then(result => res.send(result));
});


router.post("/retrieveSingle", (req, res) => {
	productController.getSingle({userId : req.body.id}).then(resultFromController => res.send(resultFromController));
});

router.put("/updating/:productId", auth.verify, (req, res) => {

	productController.updateProduct(req.params, req.body).then(result => res.send(result));
});



router.put("/archieve/:productId", auth.verify, (req, res) => {
	productController.archieveProduct(req.params, req.body).then(result => res.send(result));
});


router.put("/unarchieve/:productId", auth.verify, (req, res) => {
	productController.unarchieveProduct(req.params, req.body).then(result => res.send(result));
});










module.exports = router;