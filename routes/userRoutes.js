const express = require("express");
const router = express.Router();

const auth = require("../auth");



const userController = require("../controllers/userController")


router.post("/register",(req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
} );


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});
router.get("/getAllActive", auth.verify, (req, res) => {

	userController.getAllActive().then(result => res.send(result));
});


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
});


router.get("/getAllAdmin", auth.verify, (req, res) => {
	userController.getAllAdmin().then(result => res.send(result));
});



router.get("/getAllUser", auth.verify, (req, res) => {
	userController.getAllUser().then(result => res.send(result));
})

router.post("/retrieveSingleUser", (req, res) => {
	userController.getSingleUser({userId : req.body.id}).then(resultFromController => res.send(resultFromController));
});

router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}

	userController.enroll(data).then(result => res.send(result));
});

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	console.log(req.headers.authorization)

	userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController))
});




router.get("/userId", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		
	}

	userController.getDetails(data).then(result => res.send(result));
});



module.exports = router;