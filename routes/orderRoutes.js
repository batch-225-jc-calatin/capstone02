const express = require("express");
const router = express.Router();
const auth = require("../auth");

const orderController = require("../controllers/orderController");

const userController = require('../controllers/userController');

router.post('/createOrder', auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
    userId: auth.decode(req.headers.authorization).id,
    products: req.body
  }

  orderController.createOrder(data).then((result) => {
    res.send(result)
  })

})

module.exports = router;


